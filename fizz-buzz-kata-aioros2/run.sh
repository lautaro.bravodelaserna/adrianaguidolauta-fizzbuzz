#!/bin/sh
VERTX_OPTS="-Dvertx.logger-delegate-factory-class-name=io.vertx.core.logging.SLF4JLogDelegateFactory"

exec java $JAVA_OPTS $VERTX_OPTS -jar app.jar