# Kotlin Template Service

- [Kotlin Template Service](#kotlin-template-service)
- [Motivation](#motivation)
- [Prerequisites](#prerequisites)
- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
  - [Http Folder](#http-folder)
  - [Core Folder](#core-folder)
  - [.k8s folder](#k8s-folder)
- [Testing](#testing)
- [Code coverage](#code-coverage)
- [Delivery](#delivery)
  - [CI Configuration](#ci-configuration)
  - [Pipeline](#pipeline)
  - [Environment Variables](#environment-variables)
  - [Workflow](#workflow)
  - [Naming convention in ingress files.](#naming-convention-in-ingress-files)
  - [Kustomize support](#kustomize-support)
- [Template Endpoints](#template-endpoints)
- [Dependencies](#dependencies)


# Motivation

Standardize and speed up microservices development providing a ready to run http application with an easy example to follow.

# Prerequisites

* [JDK 8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) installed
* [IntelliJ Spek plugin](https://plugins.jetbrains.com/plugin/10915-spek-framework) installed

# Getting Started

* Clone this repository.
* Run `./service-template-cli.sh` and follow the tool instructions.
* Run gradle run (buscar forma de configurarlo) or go to `src/main/kotlin/your.package.name/http/Application.kt` and run the main method.

Your server should be now running on localhost:8080

* Try it calling this http://localhost:8080/my-example/greet/en (sacar de la url del handler el nombre del service)

# Project Structure

![](.readme-resources/project-structure.png)

* **http:**: You should place here everything related to the delivery layer.
* **core**: You should place here the business code.
* **config**: You should place here any configuration file needed to run the application. (preguntar de pasar config a http)

## Http Folder

![](.readme-resources/http-folder.png)

* **Application**: Main class where the server starts.
* **Routes**: Class to register your handlers.
* **ServerVerticle**: Main Application Vertx Verticle.
* **Error folder**: Any error related to delivery layer should be inside this folder.
* **Handler folder**: Place your handlers under this folder.
* **Provider folder**: Place your providers under this folder.

## Core Folder

![](.readme-resources/core-folder.png)

* **Action folder**: Place here the use cases of your domain.
* **Domain folder**: Place here your domain objects, entities, values and services.
* **Infrastructure Folder**: Place here any implementation of your domain services and repositories.

## .k8s folder
.k8s folder contains infrastructure information about your project. At the moment of pushing or tagging your repository the ci pipeline will get the staging/production folder and merge it with the infrastructure repository. Therefore these new modifications will be applied automatically on kubernetes cluster.


- `k8s-deploy` tool used by gitlab-ci to deploy the project.
- `base/` Common files shared by the different environments.
    
    - **kustomization.yml** list the resources that kustomize should be applied when it's called.
    - `manifests/`
        - **deployment.yml:** provides declarative updates for Pods and ReplicaSets.
        - **service.yml:** abstraction which defines a logical set of Pods and a policy by which to access them
        - **ingress-*.yml:** API object that manages external access to the services in a cluster, typically HTTP.
        - **service-monitor.yml:** define metric endpoint autoconfiguration for prometheus monitoring

- `staging/production folder`
    - **kustomization.yml** Describes which patches should be applied and how.
    - **deployment.yml** Deployment patch file with particular information of the environment.
    - **ingress-host-patch.yml** Patch for HOST replacement in ingress files.

# Testing
We decided to use ***Spek*** as our default testing framework due to its expressiveness and ease to use. The style adopted by this project is [Gherkin]('https://spekframework.org/gherkin/').

You can find a test example under `src/test/kotlin/your.package.name/TemplateTest.kt`

# Code coverage
This template is already prepared to report code coverage, but is disabled by default. In order to enable it, you need to add the following to the `.gitlab-ci.yml` under the `include` section:

```yaml
  - project: 'etermax/platform/util/ci-cd-templates'
    ref: 1.3.0
    file: 'jacoco-coverage-template.yml'
```

Then, go to `Settings > General` in the repo, expand the badges section and add the following information for the pipeline badge:

```
Name: Pipeline
Link: https://gitlab.com/%{project_path}
Badge Image: https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg
```

Add the following information for the coverage badge:
```
Name: Coverage
Link: https://gitlab.com/%{project_path}
Badge Image: https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg
```

Lastly in `Settings > CI/CD`, under `General pipelines > Test coverage parsing`, add the following regex:
```
Total.*?([0-9]{1,3})%
```

# Delivery

## CI Configuration

Before running CI in some project that's using `k8s-deploy-cli` such as `kotlin-template` (or any `kotlin-template` based project), you must ensure to have configured these environment variables at group level.

* `BACKEND_CI_IMAGE` registry.gitlab.com/etermax/platform/util/k8s-deploy-cli:2.7.0-ci
* `SLACK_DEPLOYS_URL` your webhook url
* `GITLAB_API_URL` https://gitlab.com/api/v4
* `KUBERNETES_REPOSITORY_PRODUCTION` **production repository path** e.g etermax/platform/infrastructure/production/kubernetes
* `KUBERNETES_REPOSITORY_STAGING` **staging repository path** e.g etermax/platform/infrastructure/staging/kubernetes

## Pipeline
Kotlin Template `v2.0.1` has a cleaner pipeline than previous versions thanks to the features that gitlab.com provides. Please check that your .gitlab-ci.yml is using `k8s-deploy-cli v2.7.0+` image if you want to use this new pipeline.

![](.readme-resources/pipeline.png)

You always push to master, you don't need to tag anymore if you want to go in production. With the new `promote` job you can push your code to production from any commit, you just need to change the version in gradle.properties file to the new version you want to deploy.

For this version, we're using [gitlab-ci templates](https://gitlab.com/etermax/platform/util/ci-cd-templates) so in .gitlab-ci.yml you'll see that we're including two different template files with the basic stages to run your project. Of course you can change the basic pipeline using the [extends](https://docs.gitlab.com/ee/ci/yaml/#extends) feature.

If you want to know how our ci-cd templates are made or if you want to see how to extend the pipeline please check the [ci-cd templates repository readme](https://gitlab.com/etermax/platform/util/ci-cd-templates)

## Environment Variables
```
PORT /* Listen HTTP port. Default: 8080 */
ENABLE_SENTRY /* 'true' or 'false'. Default: 'false' */
SENTRY_DSN /* DSN configured for Sentry to send logs from your app*/
```

## Workflow
```mermaid
graph LR;
    A[local]-->|git push |H(Project Repository)
    H-->B((gitlab CI))
    B-->|tag|C[production]
    B-->D[staging]
    D-->|DEPLOY|F((k8s-deploy))
    C-->|DEPLOY|F((k8s-deploy))
    F-->|merged content & commit|G[platform / infrastructure]
    G-->|manifest content|F((k8s-deploy))
    F-->I(Slack Notification #platform-deploys)
```

## Naming convention in ingress files.
`ingress-public.yml` and `ingress-private.yml` are the files we encourage you to use in your project when you're working with ingress files. `ingress-private.yml` will be validated using the **ipwhitelist** policy while `ingress-public.yml` will gain open access using the **always_true** policy.

## Kustomize support
Kotlin-Template v1.1.0 supports [Kustomize](https://kustomize.io) by default. For that reason the .k8s folder structure is a little bit different from previous version. Kustomize is a tool that helps you configuring, customizing and reusing your configuration yaml files.

Note: If for some reason you need to support the previous structure of .k8s you should use k8s-deploy `--disable-kustomize` flag.

# Template Endpoints

The template provides endpoints that kubernetes will use to gather metrics and check the service health.

```
GET /health
```
> Response **200 OK**
```json
{
  "status": "healthy"
}
```

---

```
GET /metrics
```
> Response **200 OK**
```plain/text
# HELP vertx_http_server_bytesSent Number of bytes sent by the server
# TYPE vertx_http_server_bytesSent summary
vertx_http_server_bytesSent_count 1.0
vertx_http_server_bytesSent_sum 36.0
# HELP vertx_http_server_bytesSent_max Number of bytes sent by the server
# TYPE vertx_http_server_bytesSent_max gauge
vertx_http_server_bytesSent_max 36.0
...
```

---

Also there are some example endpoints that you can delete/replace.

```
GET /info
```
> Response **200 OK**
```json
[
  {
    "name": "gradle",
    "version": "5.4.1",
    "url": "https://github.com/gradle/gradle/releases/tag/v5.4.1"
  },
  {
    "name": "Kotlin",
    "version": "1.3.31",
    "url": "https://github.com/JetBrains/kotlin/releases/tag/v1.3.31"
  },
  {
    "name": "Spek",
    "version": "2.0.2",
    "url": "https://github.com/spekframework/spek/releases/tag/2.0.2"
  },
  {
    "name": "Log4J",
    "version": "2.11.2",
    "url": "https://github.com/apache/logging-log4j2/releases/tag/log4j-2.11.2"
  },
  {
    "name": "Vertx",
    "version": "3.7.0",
    "url": "https://github.com/eclipse-vertx/vert.x/releases/tag/3.7.0"
  },
  {
    "name": "RxKotlin",
    "version": "2.3.0",
    "url": "https://github.com/ReactiveX/RxKotlin/releases/tag/2.3.0"
  },
  {
    "name": "Micrometer Metrics Prometheus",
    "version": "1.1.4",
    "url": "https://github.com/micrometer-metrics/micrometer/releases/tag/v1.1.4"
  }
]
```

---

```
GET /kotlin-template/greet/:lang 

Accepted lang values -> "en"
```

> Response **200 OK**
```json
{
    "greeting": "Hello, Platform"
}
```

> Response **404 Not Found**
```json
{
  "error": "Greetings Not Found"
}
```

# Dependencies
| Name       | Version |
|------------|---------|
| Gradle     | [5.4.1](https://github.com/gradle/gradle/releases/tag/v5.4.1)   |
| Kotlin     | [1.3.31](https://github.com/JetBrains/kotlin/releases/tag/v1.3.31)  |
| Spek       | [2.0.2](https://github.com/spekframework/spek/releases/tag/2.0.2)   |
| Log4J      | [2.11.2](https://github.com/apache/logging-log4j2/releases/tag/log4j-2.11.2)  |
| Vertx      | [3.7.0](https://github.com/eclipse-vertx/vert.x/releases/tag/3.7.0)   |
| RxKotlin   | [2.3.0](https://github.com/ReactiveX/RxKotlin/releases/tag/2.3.0)   |
| Micrometer Metrics Prometheus | [1.1.4](https://github.com/micrometer-metrics/micrometer/releases/tag/v1.1.4)   |