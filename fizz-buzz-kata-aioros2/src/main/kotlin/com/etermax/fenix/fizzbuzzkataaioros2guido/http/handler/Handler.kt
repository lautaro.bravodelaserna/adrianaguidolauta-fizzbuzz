package com.etermax.fenix.fizzbuzzkataaioros2guido.http.handler

import io.vertx.reactivex.ext.web.Router

interface Handler {
    fun register(router: Router)
}