package com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain.error

class GreetingsNotFoundException : RuntimeException("Greetings Not Found")
