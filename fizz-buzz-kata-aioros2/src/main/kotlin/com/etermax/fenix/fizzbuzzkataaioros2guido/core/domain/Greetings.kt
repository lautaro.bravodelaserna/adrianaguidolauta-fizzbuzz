package com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain

import io.reactivex.Single

interface Greetings {
    fun find(lang: String) : Single<String>
}