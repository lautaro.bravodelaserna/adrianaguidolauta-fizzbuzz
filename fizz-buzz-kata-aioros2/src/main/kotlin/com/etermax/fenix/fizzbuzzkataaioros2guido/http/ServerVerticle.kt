package com.etermax.fenix.fizzbuzzkataaioros2guido.http

import com.etermax.fenix.fizzbuzzkataaioros2guido.config.Banner
import com.etermax.fenix.fizzbuzzkataaioros2guido.config.Environment
import io.vertx.core.http.HttpServerOptions
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.core.http.HttpServer
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.handler.BodyHandler
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(ServerVerticle::class.java)

class ServerVerticle : AbstractVerticle() {
    override fun start() {
        val router = createRouter()

        startHttpServer(router)
    }

    private fun createRouter(): Router {
        return Router.router(vertx)
            .apply(this::addBodyHandler)
            .apply(Routes::register)
    }

    private fun startHttpServer(router: Router) {
        vertx.createHttpServer(HttpServerOptions().setHandle100ContinueAutomatically(true))
            .requestHandler(router)
            .rxListen(Environment.PORT)
            .subscribe(this::onServerStarted, this::onServerStartError)
    }

    private fun addBodyHandler(router: Router) {
        router.route().handler(BodyHandler.create())
    }

    private fun onServerStarted(httpServer: HttpServer) {
        Banner.print()
        println("Made with ♥ by the Platform Team at Etermax")
        println("\nServer listening on :${httpServer.actualPort()}")
    }

    private fun onServerStartError(error: Throwable) {
        logger.info("Server not started")
        logger.error(error.message, error)
    }
}