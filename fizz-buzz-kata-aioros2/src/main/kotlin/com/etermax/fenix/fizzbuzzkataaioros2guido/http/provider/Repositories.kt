package com.etermax.fenix.fizzbuzzkataaioros2guido.http.provider

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.infrastructure.repository.InMemoryGreetings

object Repositories {
    val greetings by lazy { InMemoryGreetings() }
}