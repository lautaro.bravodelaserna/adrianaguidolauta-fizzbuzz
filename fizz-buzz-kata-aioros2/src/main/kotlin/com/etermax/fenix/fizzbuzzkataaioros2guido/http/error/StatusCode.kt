package com.etermax.fenix.fizzbuzzkataaioros2guido.http.error

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain.error.GreetingsNotFoundException

fun statusCodeFrom(error: Throwable) = when (error) {
    is GreetingsNotFoundException -> 404
    else -> 500
}