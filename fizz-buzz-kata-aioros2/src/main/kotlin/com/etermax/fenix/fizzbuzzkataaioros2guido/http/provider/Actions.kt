package com.etermax.fenix.fizzbuzzkataaioros2guido.http.provider

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.action.FizzBuzz
import com.etermax.fenix.fizzbuzzkataaioros2guido.core.action.Greet

object Actions {
    val greet by lazy { Greet(Repositories.greetings) }
    val fizzBuzz by lazy { FizzBuzz() }
}