package com.etermax.fenix.fizzbuzzkataaioros2guido.core.action

import io.reactivex.Single

class FizzBuzz {

     fun execute(numberReceived: String) : Single<String> {
        return  Single.just(getMessageByNumber(numberReceived.toInt()))
    }

    fun getMessageByNumber(number: Int) : String
    {
        if(isMultipleOf(number, 3) && isMultipleOf(number, 5))
            return "FizzBuzz"

        if(isMultipleOf(number, 3))
            return "Fizz";

        if(isMultipleOf(number, 5))
            return "Buzz"

        return number.toString();
    }

    fun isMultipleOf(currentNumber: Int, multipleNumber: Int) : Boolean{
        return currentNumber % multipleNumber == 0;
    }
}