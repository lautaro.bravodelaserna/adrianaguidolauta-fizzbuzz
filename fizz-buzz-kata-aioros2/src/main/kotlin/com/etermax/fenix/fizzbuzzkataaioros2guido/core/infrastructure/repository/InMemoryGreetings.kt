package com.etermax.fenix.fizzbuzzkataaioros2guido.core.infrastructure.repository

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain.Greetings
import com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain.error.GreetingsNotFoundException
import io.reactivex.Single

class InMemoryGreetings : Greetings{
    override fun find(lang: String): Single<String> =
        Single.just(lang)
            .filter { it == "en" }
            .switchIfEmpty(Single.error(GreetingsNotFoundException()))
            .map { "Hello, Platform" }
}