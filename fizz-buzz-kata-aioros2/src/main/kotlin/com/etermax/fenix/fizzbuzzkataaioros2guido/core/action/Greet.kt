package com.etermax.fenix.fizzbuzzkataaioros2guido.core.action

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain.Greetings

class Greet(private val greetings: Greetings) {
    operator fun invoke(lang: String) = greetings.find(lang)
}