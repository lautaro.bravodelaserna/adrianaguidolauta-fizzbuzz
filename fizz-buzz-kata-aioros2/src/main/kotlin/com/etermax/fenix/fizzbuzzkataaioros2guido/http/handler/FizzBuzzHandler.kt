package com.etermax.fenix.fizzbuzzkataaioros2guido.http.handler

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.action.FizzBuzz
import com.etermax.fenix.fizzbuzzkataaioros2guido.http.error.statusCodeFrom
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext
import org.slf4j.LoggerFactory


private val logger = LoggerFactory.getLogger(GreetHandler::class.java)

private const val PATH = "/fizz-buzz-kata-aioros2/fizzbuzz/"


class FizzBuzzHandler(private val fizzBuzz: FizzBuzz) : Handler{
    override fun register(router: Router) {
        router.get(PATH).handler(this::handle)
    }

    private fun handle(context: RoutingContext) {

        val requestJson = context.body.toString();

        var request = Json.decodeValue(requestJson, Request::class.java);

        fizzBuzz.execute(request.number)
            .subscribe({ onSuccess(context, it) }, { onError(context, it) })
    }


    private fun onSuccess(context: RoutingContext, result: String) {
        val response = JsonObject().put("fizz_buzz_message", result).encodePrettily()

        logger.info(result)

        context.response().setStatusCode(200).end(response)
    }

    private fun onError(context: RoutingContext, error: Throwable) {
        val statusCode = statusCodeFrom(error)
        val errorResponse = JsonObject().put("error", error.localizedMessage).encodePrettily()

        logger.error(error.localizedMessage)

        context.response().setStatusCode(statusCode).end(errorResponse)
    }
}

class Request
{
    var number : String = "";
}