package com.etermax.fenix.fizzbuzzkataaioros2guido

import com.etermax.fenix.fizzbuzzkataaioros2guido.core.action.FizzBuzz
import com.etermax.fenix.fizzbuzzkataaioros2guido.core.domain.error.GreetingsNotFoundException
import io.reactivex.Single
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

object FizzBuzzTest : Spek ({

    val fizzBuzz: FizzBuzz = FizzBuzz();
    val fizzExpectedMessage: String = "Fizz";
    val buzzExpectedMessage: String = "Buzz";
    val fizzBuzzExpectedMessage: String = "FizzBuzz";

    lateinit var result: Single<String>
    var numberReceived: String = ""

    Feature("Fizz Buzz")
    {
        Scenario("Return fizz message when number is multiple of three")
        {
            Given("A expected message and a number received")
            {
                numberReceived = "3";
            }

            When("When FizzBuzz Is Executed")
            {
                result = fizzBuzz.execute(numberReceived);
            }

            Then("Then message is Fizz") {
                result.test().assertValue(fizzExpectedMessage)
            }
        }

        Scenario("Return Buzz message when number is multiple of five")
        {
            Given("A expected message and a number received")
            {
                numberReceived = "5";
            }

            When("When FizzBuzz Is Executed")
            {
                result = fizzBuzz.execute(numberReceived);
            }

            Then("Then message is Buzz") {
                result.test().assertValue(buzzExpectedMessage)
            }
        }

        Scenario("Return FizzBuzz message when number is multiple of five")
        {
            Given("A expected message and a number received")
            {
                numberReceived = "30";
            }

            When("When FizzBuzz Is Executed")
            {
                result = fizzBuzz.execute(numberReceived);
            }

            Then("Then message is FizzBuzz") {
                result.test().assertValue(fizzBuzzExpectedMessage)
            }
        }
    }
})